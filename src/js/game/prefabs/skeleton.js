
/*global Phaser*/
var Skeleton = function(game, x, y) {
  Phaser.Sprite.call(this, game, x, y, 'characters');
  this.anchor.setTo(0.5, 0.5);
  game.physics.arcade.enable(this);
  this.animations.add('skelLeft', [21, 22, 23], true);
  this.animations.add('skelRight', [33, 34, 35], true);
  this.animations.add('skelUp', [45, 46, 47], true);
  this.animations.add('skelDown', [9, 10, 11], true);
  this.scale.setTo(.8);
  game.add.existing(this);
  this.bringToTop();
  this.skelSpeed = 10
  
  this.body.velocity.y = -90;
  this.play('skelUp', 5, true);
};

Skeleton.prototype = Object.create(Phaser.Sprite.prototype);
Skeleton.prototype.constructor = Skeleton;

Skeleton.prototype.update = function() {
  var d100 = this.game.rnd.integerInRange(1, 100);
  var d4 = this.game.rnd.integerInRange(1, 4);
  if( d100 <= 6) {
    switch(d4) {
      case 1:
        //go up
        this.body.velocity.y = -this.skelSpeed;
        this.body.velocity.x = 0
        this.play('skelUp', 5, true);
        break;
      case 2:
        //go down
        this.body.velocity.y = this.skelSpeed;
        this.body.velocity.x = 0
        this.play('skelDown', 5, true);
        break;
      case 3:
        //go right
        this.body.velocity.x = this.skelSpeed;
        this.body.velocity.y = 0;
        this.play('skelRight', 5, true);
        break
      default:
        //go left
        this.body.velocity.x = -this.skelSpeed;
        this.body.velocity.y = 0;
        this.play('skelDown', 5, true);
}
  }
};

module.exports = Skeleton;
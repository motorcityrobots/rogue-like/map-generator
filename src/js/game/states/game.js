var game = {};
var Skeleton = require('../prefabs/skeleton.js');
/*global ROT*/
/*global Phaser*/
/*global Skeleton*/
game.create = function () {
  var w = 40, h = 40;
  var draw = new Array();
  
  //The tiles used in the map.
  var blankTile = 22;
  var floorTile = 14;
  var doorTile = 48;


  
  //Populate map with blank space.
  for (var i = 0; i < w; i++) {
    draw[i] = new Array();
    for (var j = 0; j < h; j++) {
      draw[i][j] = blankTile;
    }
  }

  var map = new ROT.Map.Digger(w, h, corridorLength = [5]);
  
  var digCallback = function(x, y, value) {
    if(value) {
      this[y][x] = blankTile;
    } else {
      this[y][x] = floorTile;
    }
  }
  
  map.create(digCallback.bind(draw));


  
  //This isn't going to work with the current tiles?
  //Everything is either a blank tile or a floor tile at this point.
  //We can only look up or left for other wall tiles.
  // for( var y = 1; y < h - 1; y++ ) {
  //   for( var x = 1; x < w - 1; x++ ) {
  //     var up = draw[y - 1][x],
  //         down = draw[y + 1][x],
  //         left = draw[y][x - 1],
  //         right = draw[y][x + 1],
  //         upLeft = draw[y - 1][x - 1],
  //         upRight = draw[y - 1][x + 1],
  //         downLeft = draw[y + 1][x - 1],
  //         downRight = draw[y + 1][x + 1];
        
  //     if(draw[y][x] == blankTile){
  //       //Draw all the top and bottom walls
  //       if( down == floorTile) {
  //         draw[y][x] = topWallTile;
  //       }
  //       if(up == floorTile) {
  //         draw[y][x] = bottomWallTile;
  //       }
  //       //Draw all the right
  //       if(left == floorTile) {
  //         draw[y][x] = sideRightWallTile;
  //       }
  //       //Draw all the left
  //       if(right == floorTile) {
  //         draw[y][x] = sideLeftWallTile;
  //       }
        
        
  //     }
  //   }
  // }

  
  //Convert array to csv
  var csvContent = "";
  draw.forEach(function(infoArray, index){
    var dataString = infoArray.join(",");
    csvContent += index < draw.length ? dataString+ "\n" : dataString;
  });
  // console.log(csvContent)
  
  this.game.cache.addTilemap('map', null, csvContent, Phaser.Tilemap.CSV);
  
  this.map = this.game.add.tilemap('map', 16, 16);
  this.map.addTilesetImage('basictiles');
  
  this.layer = this.map.createLayer(0);
  this.layer.resizeWorld();

  this.layer2 = this.map.createBlankLayer('level2', w, h, 16, 16);
  this.layer3 = this.map.createBlankLayer('level3', w, h, 16, 16);
  
    //Add the doors to the map
  var drawDoor = function(x, y) {
    draw[y][x] = floorTile;
    this.map.putTile(doorTile, x, y, this.layer2);
  };
  var rooms = map.getRooms();
  for (var r = 0; r < rooms.length; r++) {
    var room = rooms[r];
    room.getDoors(drawDoor.bind(this));
  }

  this.map.setCollision([blankTile], true, this.layer);
  this.game.physics.startSystem(Phaser.Physics.ARCADE);
  
  this.cursors = this.game.input.keyboard.createCursorKeys();
  
  this.player = this.game.add.sprite((rooms[0].getLeft() + rooms[0].getRight()) / 2 * 16, (rooms[0].getTop() + rooms[0].getBottom()) / 2 * 16, 'characters', 4);
  this.player.animations.add('left', [15,16,17], 10, true);
  this.player.animations.add('right', [27,28,29], 10, true);
  this.player.animations.add('up', [39,40,41], 10, true);
  this.player.animations.add('down', [3,4,5], 10, true);
  this.player.anchor.setTo(0.5);
  this.player.scale.setTo(.8);
  this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
  this.game.camera.follow(this.player);
  
  this.skeletons = this.add.group();
  this.skeletons.enableBody = true;
  
  this.drawBox = function(room) {
    var boxX = game.rnd.integerInRange(room.getLeft(), room.getRight());
    var boxY = game.rnd.integerInRange(room.getTop(), room.getBottom());
    this.map.putTile(36, boxX, boxY, this.layer3);
    
    var skelX = game.rnd.integerInRange(room.getLeft(), room.getRight());
    var skelY = game.rnd.integerInRange(room.getTop(), room.getBottom());
    var skeleton = this.skeletons.getFirstExists(false);
    if(!skeleton){
      skeleton = new Skeleton(this.game, skelX * 16, skelY * 16);
      this.skeletons.add(skeleton);
    }else{
      skeleton.reset(skelX, skelY);
    }
  }
  
  for (var r = 0; r < rooms.length; r++) {
    var room = rooms[r];
    this.drawBox(room);
  }
};

game.update = function() {
  
  this.game.physics.arcade.collide(this.player, this.layer);
  this.game.physics.arcade.collide(this.skeletons, this.layer);
  this.player.body.velocity.set(0);

  if (this.cursors.up.isDown)
  {
      this.player.body.velocity.y -= 100;
      this.player.play('up');
  }
  else if (this.cursors.down.isDown)
  {
      this.player.body.velocity.y += 100;
      this.player.play('down');
  }

  else if (this.cursors.left.isDown)
  {
      this.player.body.velocity.x -= 100;
      this.player.play('left');
  }
  else if (this.cursors.right.isDown)
  {
      this.player.body.velocity.x += 100;
      this.player.play('right');
  }
  else {
    this.player.animations.stop();
  }
};

game.render = function () {

    this.game.debug.cameraInfo(game.camera, 32, 32);
    // this.game.debug.body(this.player);
};

module.exports = game;

var preloader = {};

preloader.preload = function () {
  this.game.load.image('logo', 'images/phaser.png');
  this.game.load.image('dungeontile', 'images/dungeontile.png');
  this.game.load.image('dungeoneer', 'images/dungeoneer.png');
  
  this.game.load.image('basictiles', 'images/basictiles.png');
  this.game.load.spritesheet('characters', 'images/characters.png', 16, 16);
};

preloader.create = function () {
  this.game.state.start('game');
};

module.exports = preloader;
